<?php

use Illuminate\Support\Facades\Route;
use File\Infrastructure\Controllers\FileController;

Route::prefix('files')->group(function () {
    Route::post('store', [FileController::class, 'store'])->name('files.store');
    Route::get('{id}/get-file', [FileController::class, 'getFile'])->name('files.getFile');
    Route::get('{id}/get-content', [FileController::class, 'getContent'])->name('files.getContent');
    Route::post('{id}/update', [FileController::class, 'update'])->name('files.update');
    Route::delete('{id}/delete', [FileController::class, 'delete'])->name('files.delete');
});
