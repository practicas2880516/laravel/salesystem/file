<?php

namespace File\Application\Services;

use File\Application\Interfaces\Services\ManagerFileServiceInterface;
use File\Application\Interfaces\Services\StorageFileServiceInterface;
use File\Domain\Dto\File\FileNewDto;
use File\Domain\Dto\File\FileUpdateDto;
use File\Domain\Dto\File\UpdateFileDto;
use File\Domain\Exceptions\FileNotfoundException;
use File\Infrastructure\Interfaces\Repositories\Files\FileRepositoryInterface;
use File\Infrastructure\Providers\Dto\S3\FileCreatedDto;
use File\Infrastructure\Providers\Dto\S3\FileDeleteDto;
use File\Infrastructure\Providers\Dto\S3\FileGetContentDto;
use File\Infrastructure\Providers\Dto\S3\FileReplaceDto;
use File\Infrastructure\Providers\Dto\S3\FileUpdatedDto;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;

class ManagerFileService implements ManagerFileServiceInterface
{
    /**
     * @var FileCreatedDto|null
     */
    private ?FileCreatedDto $createdFile;

    /**
     * @var FileUpdatedDto|null
     */
    private ?FileUpdatedDto $updatedFile;

    /**
     * @var object
     */
    private ?object $file;

    /**
     * @var StorageFileServiceInterface
     */
    private StorageFileServiceInterface $storageFileService;

    /**
     * @var FileRepositoryInterface
     */
    private FileRepositoryInterface $fileRepo;

    /**
     * @param StorageFileServiceInterface $storageFileService
     * @param FileRepositoryInterface $fileRepo
     */
    public function __construct(
        StorageFileServiceInterface $storageFileService,
        FileRepositoryInterface $fileRepo
    )
    {
        $this->storageFileService = $storageFileService;
        $this->fileRepo = $fileRepo;
    }

    /**
     * @param int $id
     * @return object
     * @throws \Throwable
     */
    public function getFile(int $id): object
    {
        $this->findById($id);

        unset($this->file->disk_file, $this->file->path_file);

        return $this->file;
    }

    /**
     * @param int $id
     * @return string
     * @throws \Throwable
     */
    public function getContentFile(int $id):string
    {
        $this->findById($id);

        $dto = new FileGetContentDto();
        $dto->diskFile = $this->file->disk_file;
        $dto->pathFile = $this->file->path_file;

        return base64_encode($this->storageFileService->getContent($dto));
    }

    /**
     * @param UploadedFile $file
     * @param string|null $name
     * @return int
     */
    public function storeFile(UploadedFile $file, string $name = null):int
    {
        $this->createdFile = $this->storageFileService
            ->store($file)
            ->getCreatedFile();

        return $this->store($name);
    }

    /**
     * @param string $name
     * @return int
     */
    protected function store(string $name = null):int
    {
        $dto = new FileNewDto();
        $dto->diskFile = $this->createdFile->diskFile;
        $dto->name = $name;
        $dto->pathFile = $this->createdFile->pathFile;
        $dto->originalName = $this->createdFile->originalName;
        $dto->updatedName = $this->createdFile->updatedName;
        $dto->size = $this->createdFile->size;
        $dto->extension = $this->createdFile->extension;

        return $this->fileRepo
            ->setUser(auth()->user())
            ->store($dto)
            ->getFileId();
    }

    /**
     * @param UploadedFile $file
     * @param int $id
     * @return $this
     * @throws \Throwable
     */
    public function updateFile(UpdateFileDto $updateFileDto):self
    {
        $this->findById($updateFileDto->id);

        $dto = (App::make(FileReplaceDto::class));
        $dto->pathFile = $this->file->path_file;
        $dto->diskFile = $this->file->disk_file;
        $dto->file = $updateFileDto->file;

        $this->updatedFile = $this->storageFileService
            ->update($dto)
            ->getUpdatedFile();

        $this->update($updateFileDto->name);

        return $this;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    protected function update(string $name = null):self
    {
        $dto = (App::make(FileUpdateDto::class));
        $dto->id = $this->file->id;
        $dto->name = $name ?? $this->file->name;
        $dto->originalName = $this->updatedFile->originalName;
        $dto->size = $this->updatedFile->size;
        $dto->extension = $this->updatedFile->extension;

        $this->fileRepo
            ->setUser(auth()->user())
            ->update($dto);

        return $this;
    }

    /**
     * @param int $id
     * @return $this
     * @throws \Throwable
     */
    public function deleteFile(int $id):self
    {
        $this->findById($id);

        $dto = (App::make(FileDeleteDto::class));
        $dto->diskFile = $this->file->disk_file;
        $dto->pathFile = $this->file->path_file;

        $this->storageFileService
            ->delete($dto);

        $this->fileRepo
            ->setUser(auth()->user())
            ->delete($id);

        return $this;
    }

    /**
     * @param int $id
     * @return $this
     * @throws \Throwable
     */
    protected function findById(int $id):self
    {
        $this->file = $this->fileRepo
            ->setUser(auth()->user())
            ->findById($id);

        throw_if(is_null($this->file), new FileNotfoundException());

        return $this;
    }
}
