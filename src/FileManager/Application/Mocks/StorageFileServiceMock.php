<?php

namespace File\Application\Mocks;

use File\Application\Interfaces\Services\StorageFileServiceInterface;
use File\Infrastructure\Providers\Dto\S3\FileCreatedDto;
use File\Infrastructure\Providers\Dto\S3\FileUpdatedDto;
use Illuminate\Support\Str;

class StorageFileServiceMock
{
    public function generateGetContentWorking()
    {
        $mock = \Mockery::mock(StorageFileServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('getContent')
            ->once()
            ->andReturn('Contedido del archivo');

        return $mock;
    }

    public function generateStoreWorking()
    {
        $mock = \Mockery::mock(StorageFileServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('store')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getCreatedFile')
            ->once()
            ->andReturnUsing(function (){
                $dto = new FileCreatedDto();
                $dto->diskFile = Str::random(15);
                $dto->pathFile = Str::random(30);
                $dto->originalName = Str::random(30);
                $dto->updatedName = Str::random(30);
                $dto->extension = Str::random(3);
                $dto->size = random_int(0, 5000);
                return $dto;
            });

        return $mock;
    }

    public function generateUpdateWorking()
    {
        $mock = \Mockery::mock(StorageFileServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getUpdatedFile')
            ->once()
            ->andReturnUsing(function (){
                $dto = new FileUpdatedDto();
                $dto->originalName = Str::random(30);
                $dto->extension = Str::random(3);
                $dto->size = random_int(0, 5000);
                return $dto;
            });

        return $mock;
    }

    public function generateDeleteWorking()
    {
        $mock = \Mockery::mock(StorageFileServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('delete')
            ->once()
            ->andReturnSelf();

        return $mock;
    }
}
