<?php

namespace File\Application\Mocks;

use File\Application\Interfaces\Services\ManagerFileServiceInterface;
use Illuminate\Support\Str;

class ManagerFileServiceMock
{
    public function generateGetFileWorking()
    {
        $mock = \Mockery::mock(ManagerFileServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('getFile')
            ->once()
            ->andReturnUsing(function () {
                $obj = new \stdClass();
                $obj->id = 1;
                $obj->name = Str::random(10);
                $obj->original_name = Str::random(30);
                $obj->updated_name = Str::random(30);
                $obj->extension = Str::random(5);
                $obj->size = random_int(0, 5000);
                return $obj;
            });

        return $mock;
    }

    public function generateGetContentFileWorking()
    {
        $mock = \Mockery::mock(ManagerFileServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('getContentFile')
            ->once()
            ->andReturn(base64_encode('Contenido del archivo'));

        return $mock;
    }

    public function generateStoreFileWorking()
    {
        $mock = \Mockery::mock(ManagerFileServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('storeFile')
            ->once()
            ->andReturn(1);

        return $mock;
    }

    public function generateUpdateFileWorking()
    {
        $mock = \Mockery::mock(ManagerFileServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('updateFile')
            ->once()
            ->andReturnSelf();

        return $mock;
    }

    public function generateDeleteWorking()
    {
        $mock = \Mockery::mock(ManagerFileServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('deleteFile')
            ->once()
            ->andReturnSelf();

        return $mock;
    }
}
