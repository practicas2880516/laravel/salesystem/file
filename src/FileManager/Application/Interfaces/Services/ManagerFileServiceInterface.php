<?php

namespace File\Application\Interfaces\Services;

use File\Domain\Dto\File\UpdateFileDto;
use Illuminate\Http\UploadedFile;

interface ManagerFileServiceInterface
{

    /**
     * @param int $id
     * @return object
     */
    public function getFile(int $id):object;

    /**
     * @param int $id
     * @return string
     */
    public function getContentFile(int $id):string;

    /**
     * @param UploadedFile $file
     * @param string|null $name
     * @return int
     */
    public function storeFile(UploadedFile $file, string $name = null):int;

    /**
     * @param array $data
     * @param int $id
     * @return self
     */
    public function updateFile(UpdateFileDto $updateFileDto):self;

    /**
     * @param int $id
     * @return self
     */
    public function deleteFile(int $id):self;
}
