<?php

namespace File\Application\Interfaces\Services;

use File\Infrastructure\Providers\Dto\S3\FileCreatedDto;
use File\Infrastructure\Providers\Dto\S3\FileDeleteDto;
use File\Infrastructure\Providers\Dto\S3\FileGetContentDto;
use File\Infrastructure\Providers\Dto\S3\FileReplaceDto;
use File\Infrastructure\Providers\Dto\S3\FileUpdatedDto;
use Illuminate\Http\UploadedFile;

interface StorageFileServiceInterface
{
    const DISK_STORAGE = 's3';

    /**
     * @param FileGetContentDto $dto
     * @return string
     */
    public function getContent(FileGetContentDto $dto):string;

    /**
     * @return FileCreatedDto
     */
    public function getCreatedFile():FileCreatedDto;

    /**
     * @return FileUpdatedDto
     */
    public function getUpdatedFile():FileUpdatedDto;

    /**
     * @param UploadedFile $file
     * @return self
     */
    public function store(UploadedFile $file): self;

    /**
     * @param FileReplaceDto $dto
     * @return self
     */
    public function update(FileReplaceDto $dto):self;

    /**
     * @param FileDeleteDto $dto
     * @return self
     */
    public function delete(FileDeleteDto $dto):self;
}
