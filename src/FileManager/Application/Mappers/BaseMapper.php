<?php

namespace File\Application\Mappers;

use File\Domain\Dto\BaseDto;

abstract class BaseMapper
{
        /**
         * @var BaseDto
         */
        protected BaseDto $dto;

        /**
         * @return BaseDto
         */
        abstract protected function getNewDto():BaseDto;
}
