<?php

namespace File\Application\Mappers\Files;

use File\Application\Mappers\BaseMapper;
use File\Domain\Dto\File\UpdateFileDto;
use Illuminate\Http\Request;

class UpdateFileDtoMapper extends BaseMapper
{
    /**
     * @return UpdateFileDto
     */
    protected function getNewDto():UpdateFileDto
    {
        return new UpdateFileDto;
    }

    /**
     * @param Request $request
     * @return UpdateFileDto
     */
    public function updateFromRequest(Request $request):UpdateFileDto
    {
        $dto = $this->getNewDto();
        $dto->name = $request->get('name');
        $dto->file = $request->file('document');
        return $dto;
    }

}
