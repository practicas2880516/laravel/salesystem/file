<?php

namespace File\Infrastructure\Controllers;

use File\Application\Interfaces\Services\ManagerFileServiceInterface;
use File\Application\Mappers\Files\UpdateFileDtoMapper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FileController extends BaseController
{
    /**
     * @var ManagerFileServiceInterface
     */
    private ManagerFileServiceInterface $managerFileService;

    public function __construct(
        ManagerFileServiceInterface $managerFileService
    )
    {
        $this->managerFileService = $managerFileService;
    }

    public function getFile(int $id)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($id) {
            return [
                'message' => 'File',
                'file' => $this->managerFileService
                    ->getFile($id)
            ];
        });
    }

    public function getContent(int $id)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($id) {
            return [
                'message' => 'File',
                'content' => $this->managerFileService
                    ->getContentFile($id)
            ];
        });
    }

    public function store(Request $request)
    {
        $request->validate([
            'document' => ['required', 'file'],
            'name' => ['nullable', 'string'],
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request) {

            $fileId = $this->managerFileService
                ->storeFile($request->file('document'), $request->get('name'));

            return [
                'message' => 'Created File',
                'file_id' => $fileId
            ];
        });
    }

    public function update(int $id, Request $request)
    {
        $request->validate([
            'document' => ['required', 'file'],
            'name' => ['nullable', 'string'],
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($id, $request) {

            $updateFileDto = (App::make(UpdateFileDtoMapper::class))
                ->updateFromRequest($request);

            $updateFileDto->id = $id;

            $this->managerFileService->updateFile($updateFileDto);

            return [
                'message' => 'Updated File',
            ];
        });
    }

    public function delete(int $id)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($id) {

            $this->managerFileService
                ->deleteFile($id);

            return [
                'message' => 'Deleted File',
            ];
        });
    }
}
