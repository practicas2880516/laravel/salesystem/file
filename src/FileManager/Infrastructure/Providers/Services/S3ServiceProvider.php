<?php

namespace File\Infrastructure\Providers\Services;

use File\Application\Interfaces\Services\StorageFileServiceInterface;
use File\Infrastructure\Providers\Dto\S3\FileCreatedDto;
use File\Infrastructure\Providers\Dto\S3\FileDeleteDto;
use File\Infrastructure\Providers\Dto\S3\FileGetContentDto;
use File\Infrastructure\Providers\Dto\S3\FileReplaceDto;
use File\Infrastructure\Providers\Dto\S3\FileUpdatedDto;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class S3ServiceProvider implements StorageFileServiceInterface
{
    /**
     * @var UploadedFile
     */
    private UploadedFile $file;

    /**
     * @var string
     */
    private string $originalName;

    /**
     * @var string
     */
    private string $fileName;

    /**
     * @var string
     */
    private string $extension;

    /**
     * @var string
     */
    private string $pathFile;

    /**
     * @var float
     */
    public float $size;

    /**
     * @param FileGetContentDto $dto
     * @return string
     */
    public function getContent(FileGetContentDto $dto): string
    {
        return Storage::disk($dto->diskFile)
            ->get($dto->pathFile);
    }


    /**
     * @return FileCreatedDto
     */
    public function getCreatedFile():FileCreatedDto
    {
        $dto = (App::make(FileCreatedDto::class));
        $dto->diskFile = self::DISK_STORAGE;
        $dto->pathFile = $this->pathFile;
        $dto->originalName = $this->originalName;
        $dto->updatedName = $this->fileName;
        $dto->size = $this->size;
        $dto->extension = $this->extension;
        return $dto;
    }

    /**
     * @return FileUpdatedDto
     */
    public function getUpdatedFile():FileUpdatedDto
    {
        $dto = (App::make(FileUpdatedDto::class));
        $dto->originalName = $this->originalName;
        $dto->size = $this->size;
        $dto->extension = $this->extension;
        return $dto;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function store(UploadedFile $file): self
    {
        $this->file = $file;
        $this->setFileStoreDetails();
        $this->storeFileInStorage();
        return $this;
    }

    /**
     * @param FileReplaceDto $dto
     * @return self
     */
    public function update(FileReplaceDto $dto):self
    {
        $this->file = $dto->file;
        $this->setFileUpdateDetails();
        Storage::disk($dto->diskFile)->put($dto->pathFile, $this->file->getContent());
        return $this;
    }

    /**
     * @param FileDeleteDto $dto
     * @return $this
     */
    public function delete(FileDeleteDto $dto):self
    {
        Storage::disk($dto->diskFile)->delete([$dto->pathFile]);
        return $this;
    }

    /**
     * @return $this
     */
    protected function setFileStoreDetails():self
    {
        $this->originalName = $this->file->getClientOriginalName();
        $this->fileName = explode('.', $this->file->hashName())[0];
        $this->extension = $this->file->getClientOriginalExtension();
        $this->size = $this->file->getSize();
        $this->pathFile = join('/', [date('Y'), date('m'), $this->fileName]);
        return $this;
    }

    /**
     * @return $this
     */
    protected function setFileUpdateDetails():self
    {
        $this->originalName = $this->file->getClientOriginalName();
        $this->extension = $this->file->getClientOriginalExtension();
        $this->size = $this->file->getSize();
        return $this;
    }

    /**
     * @return $this
     */
    protected function storeFileInStorage():self
    {
        Storage::disk(self::DISK_STORAGE)
            ->put($this->pathFile, $this->file->getContent());
        return $this;
    }

}
