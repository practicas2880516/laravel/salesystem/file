<?php

namespace File\Infrastructure\Providers\Dto\S3;

use File\Infrastructure\Providers\Dto\BaseDto;

class FileDeleteDto extends BaseDto
{
    /**
     * @var string
     */
    public string $diskFile;

    /**
     * @var string
     */
    public string $pathFile;
}
