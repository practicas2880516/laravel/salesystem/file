<?php

namespace File\Infrastructure\Providers\Dto\S3;

use File\Infrastructure\Providers\Dto\BaseDto;

class FileCreatedDto extends BaseDto
{
    /**
     * @var string
     */
    public string $diskFile;

    /**
     * @var string
     */
    public string $pathFile;

    /**
     * @var string
     */
    public string $originalName;

    /**
     * @var string
     */
    public string $updatedName;

    /**
     * @var float
     */
    public float $size;

    /**
     * @var string
     */
    public string $extension;
}
