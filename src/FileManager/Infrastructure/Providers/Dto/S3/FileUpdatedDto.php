<?php

namespace File\Infrastructure\Providers\Dto\S3;

use File\Infrastructure\Providers\Dto\BaseDto;

class FileUpdatedDto extends BaseDto
{
    /**
     * @var string
     */
    public string $originalName;

    /**
     * @var float
     */
    public float $size;

    /**
     * @var string
     */
    public string $extension;
}
