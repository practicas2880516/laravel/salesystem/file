<?php

namespace File\Infrastructure\Repositories;

use File\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

abstract class BaseRepository implements BaseRepositoryInterface
{
    /**
     * @var object|null
     */
    protected ?object $user = null;

    /**
     * @var string
     */
    protected string $tablaName;

    /**
     * @var string
     */
    protected string $databaseConnection;

    /**
     * @param object $user
     * @return $this
     */
    public function setUser(object $user):self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName():string
    {
        return $this->tablaName;
    }

    /**
     * @return string
     */
    public function getDatabaseConnection():string
    {
        return $this->databaseConnection;
    }
}
