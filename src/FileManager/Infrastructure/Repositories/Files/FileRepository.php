<?php

namespace File\Infrastructure\Repositories\Files;

use File\Domain\Dto\File\FileNewDto;
use File\Domain\Dto\File\FileUpdateDto;
use File\Infrastructure\Interfaces\Repositories\Files\FileRepositoryInterface;
use File\Infrastructure\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class FileRepository extends BaseRepository implements FileRepositoryInterface
{
    /**
     * @var int
     */
    private int $fileId;

    /**
     * @var string
     */
    protected string $tablaName = 'file.files';

    /**
     * @var string
     */
    protected string $databaseConnection = 'pgsql';


    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id):object|null
    {
        return DB::connection($this->databaseConnection)
            ->table($this->tablaName)
            ->select([
                'id AS id',
                'name AS name',
                'disk_file AS disk_file',
                'original_name AS original_name',
                'updated_name AS updated_name',
                'path_file AS path_file',
                'extension AS extension',
                'size AS size'
            ])
            ->whereNull('deleted_at')
            ->find($id);
    }
    /**
     * @return int
     */
    public function getFileId():int
    {
        return $this->fileId;
    }
    /**
     * @param FileNewDto $dto
     * @return $this
     */
    public function store(FileNewDto $dto):self
    {
        $this->fileId = DB::connection($this->databaseConnection)
            ->table($this->tablaName)
            ->insertGetId([
                'disk_file' => $dto->diskFile,
                'path_file' => $dto->pathFile,
                'original_name' => $dto->originalName,
                'updated_name' => $dto->updatedName,
                'size' => $dto->size,
                'extension' => $dto->extension,
                'name' => $dto->name,
                'created_at' => 'now()',
                'user_who_created_id' => $this->user->id
            ]);

        return $this;
    }

    /**
     * @param FileUpdateDto $dto
     * @return $this
     */
    public function update(FileUpdateDto $dto): self
    {
        DB::connection($this->databaseConnection)
            ->table($this->tablaName)
            ->where('id', '=', $dto->id)
            ->update([
                'original_name' => $dto->originalName,
                'size' => $dto->size,
                'extension' => $dto->extension,
                'name' => $dto->name,
                'updated_at' => 'now()',
                'user_who_updated_id' => $this->user->id
            ]);

        return $this;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function delete(int $id): self
    {
        DB::connection($this->databaseConnection)
            ->table($this->tablaName)
            ->delete($id);

        return $this;
    }


}
