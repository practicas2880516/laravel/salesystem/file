<?php

namespace File\Infrastructure\Interfaces\Repositories\Files;

use File\Domain\Dto\File\FileNewDto;
use File\Domain\Dto\File\FileUpdateDto;
use File\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface FileRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @return int
     */
    public function getFileId():int;

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id):object|null;

    /**
     * @param FileNewDto $dto
     * @return self
     */
    public function store(FileNewDto $dto):self;

    /**
     * @param FileUpdateDto $dto
     * @return self
     */
    public function update(FileUpdateDto $dto):self;

    /**
     * @param int $id
     * @return self
     */
    public function delete(int $id):self;
}
