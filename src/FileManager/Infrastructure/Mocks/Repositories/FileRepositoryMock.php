<?php

namespace File\Infrastructure\Mocks\Repositories;

use File\Infrastructure\Interfaces\Repositories\Files\FileRepositoryInterface;
use Illuminate\Support\Str;

class FileRepositoryMock
{
    public function generateStoreWorking()
    {
        $mock = \Mockery::mock(FileRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('store')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getFileId')
            ->once()
            ->andReturn(1);

        return $mock;
    }

    public function generateUpdateWorking()
    {
        $mock = \Mockery::mock(FileRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->times(2)
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $obj = new \stdClass();
                $obj->id = 1;
                $obj->name = Str::random(30);
                $obj->disk_file = Str::random(15);
                $obj->original_name = Str::random(30);
                $obj->updated_name = Str::random(30);
                $obj->path_file = Str::random(30);
                $obj->extension = Str::random(3);
                $obj->size = random_int(0, 5000);
                return $obj;
            });

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }

    public function generateDeleteWorking()
    {
        $mock = \Mockery::mock(FileRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->times(2)
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $obj = new \stdClass();
                $obj->id = 1;
                $obj->name = Str::random(30);
                $obj->disk_file = Str::random(15);
                $obj->original_name = Str::random(30);
                $obj->updated_name = Str::random(30);
                $obj->path_file = Str::random(30);
                $obj->extension = Str::random(3);
                $obj->size = random_int(0, 5000);
                return $obj;
            });

        $mock->shouldReceive('delete')
            ->once()
            ->andReturnSelf();

        return $mock;
    }

    public function generateFindByIdNull()
    {
        $mock = \Mockery::mock(FileRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnNull();

        return $mock;
    }

    public function generateFindById()
    {
        $mock = \Mockery::mock(FileRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $obj = new \stdClass();
                $obj->id = 1;
                $obj->name = Str::random(30);
                $obj->disk_file = Str::random(15);
                $obj->original_name = Str::random(30);
                $obj->updated_name = Str::random(30);
                $obj->path_file = Str::random(30);
                $obj->extension = Str::random(3);
                $obj->size = random_int(0, 5000);
                return $obj;
            });

        return $mock;
    }
}
