<?php

namespace File\Test;

use App\Models\User;
use File\Infrastructure\Interfaces\Repositories\Files\FileRepositoryInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

abstract class Base extends TestCase
{
    /**
     * @var int
     */
    protected int $fileId;

    /**
     * @var string
     */
    protected string $pathFile;

    /**
     * @var FileRepositoryInterface
     */
    protected FileRepositoryInterface $fileRepo;

    /**
     * @var User
     */
    protected User $user;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @return void
     */
    protected function setNewUser():void
    {
        $this->user = new User();
        $this->user->id = 1;
    }

    /**
     * @return void
     */
    protected function createFile():void
    {
        $file = UploadedFile::fake()->image('test.png');
        $this->pathFile = 'test/' . explode('.', $file->hashName())[0];
        Storage::disk('s3')->put($this->pathFile, $file->getContent());
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function createEntityFile():void
    {
        $this->fileId = DB::connection($this->fileRepo->getDatabaseConnection())
            ->table($this->fileRepo->getTableName())
            ->insertGetId([
                'disk_file' => 's3',
                'name' => Str::random(10),
                'path_file' => Str::random(30),
                'original_name' => Str::random(30),
                'updated_name' => Str::random(30),
                'extension' => Str::random(5),
                'size' => random_int(0, 5000),
            ]);
    }

    /**
     * @return void
     */
    protected function setRepositories():void
    {
        $this->fileRepo = (App::make(FileRepositoryInterface::class));
    }
}
