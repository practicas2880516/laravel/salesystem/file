<?php

namespace File\Test\Application\Services;

use File\Application\Interfaces\Services\ManagerFileServiceInterface;
use File\Application\Interfaces\Services\StorageFileServiceInterface;
use File\Application\Mocks\StorageFileServiceMock;
use File\Domain\Dto\File\UpdateFileDto;
use File\Infrastructure\Interfaces\Repositories\Files\FileRepositoryInterface;
use File\Infrastructure\Mocks\Repositories\FileRepositoryMock;
use File\Test\Base;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;

class ManagerFileServiceTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
    }

    /**
     * @test
     */
    public function isGetFileWorking()
    {
        $this->instance(
            FileRepositoryInterface::class,
            (App::make(FileRepositoryMock::class))->generateFindById()
        );

        $this->actingAs($this->user);

        $file = (App::make(ManagerFileServiceInterface::class))
            ->getFile(1);

        $this->assertIsObject($file);
    }

    /**
     * @test
     */
    public function isGetFileFailing()
    {
        $this->instance(
            FileRepositoryInterface::class,
            (App::make(FileRepositoryMock::class))->generateFindByIdNull()
        );

        try {
            $this->actingAs($this->user);

            (App::make(ManagerFileServiceInterface::class))
                ->getFile(1);
        } catch (\Exception $exception) {
            $this->assertTrue($exception->getCode() === 404);
        }
    }

    /**
     * @test
     */
    public function isGetContentFileWorking()
    {
        $this->instance(
            FileRepositoryInterface::class,
            (App::make(FileRepositoryMock::class))->generateFindById()
        );

        $this->instance(
            StorageFileServiceInterface::class,
            (App::make(StorageFileServiceMock::class))->generateGetContentWorking()
        );

        $this->actingAs($this->user);

        $content = (App::make(ManagerFileServiceInterface::class))
            ->getContentFile(1);

        $this->assertNotNull($content);
    }

    /**
     * @test
     */
    public function isGetContentFileFailing()
    {
        $this->instance(
            FileRepositoryInterface::class,
            (App::make(FileRepositoryMock::class))->generateFindByIdNull()
        );


        try {
            $this->actingAs($this->user);

            (App::make(ManagerFileServiceInterface::class))
                ->getFile(1);
        } catch (\Exception $exception) {
            $this->assertTrue($exception->getCode() === 404);
        }
    }

    /**
     * @test
     */
    public function isStoreFileWorking()
    {
        $this->instance(
            StorageFileServiceInterface::class,
            (App::make(StorageFileServiceMock::class))->generateStoreWorking()
        );

        $this->instance(
            FileRepositoryInterface::class,
            (App::make(FileRepositoryMock::class))->generateStoreWorking()
        );

        $this->actingAs($this->user);

        $file = UploadedFile::fake()->image('test.png');

        $fileId = (App::make(ManagerFileServiceInterface::class))
            ->storeFile($file, 'File name');

        $this->assertIsInt($fileId);
    }

    /**
     * @test
     */
    public function isUpdateFileWorking()
    {
        $this->instance(
            StorageFileServiceInterface::class,
            (App::make(StorageFileServiceMock::class))->generateUpdateWorking()
        );

        $this->instance(
            FileRepositoryInterface::class,
            (App::make(FileRepositoryMock::class))->generateUpdateWorking()
        );

        $this->actingAs($this->user);

        $dto = new UpdateFileDto();
        $dto->id = 1;
        $dto->name = 'File name';
        $dto->file = UploadedFile::fake()->image('test.png');

        (App::make(ManagerFileServiceInterface::class))
            ->updateFile($dto);

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function isUpdateFileFailing()
    {
        $this->instance(
            FileRepositoryInterface::class,
            (App::make(FileRepositoryMock::class))->generateFindByIdNull()
        );

        try {
            $this->actingAs($this->user);

            $dto = new UpdateFileDto();
            $dto->id = 1;
            $dto->name = 'File name';
            $dto->file = UploadedFile::fake()->image('test.png');

            (App::make(ManagerFileServiceInterface::class))
                ->updateFile($dto);
        } catch (\Exception $exception) {
            self::assertTrue($exception->getCode() === 404);
        }
    }

    /**
     * @test
     */
    public function isDeleteFileWorking()
    {
        $this->instance(
            StorageFileServiceInterface::class,
            (App::make(StorageFileServiceMock::class))->generateDeleteWorking()
        );

        $this->instance(
            FileRepositoryInterface::class,
            (App::make(FileRepositoryMock::class))->generateDeleteWorking()
        );

        $this->actingAs($this->user);

        (App::make(ManagerFileServiceInterface::class))
            ->deleteFile(1);

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function isDeleteFailing()
    {
        $this->instance(
            FileRepositoryInterface::class,
            (App::make(FileRepositoryMock::class))->generateFindByIdNull()
        );

        try {
            $this->actingAs($this->user);
            (App::make(ManagerFileServiceInterface::class))
                ->deleteFile(1);
        } catch (\Exception $exception) {
            self::assertTrue($exception->getCode() === 404);
        }
    }
}
