<?php

namespace File\Test\Infrastructure\Repositories\Files;

use File\Domain\Dto\File\FileNewDto;
use File\Domain\Dto\File\FileUpdateDto;
use File\Test\Base;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;

class FileRepositoryTest extends Base
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
        $this->setRepositories();
        $this->beginDatabaseTransaction();
    }

    /**
     * @test
     */
    public function isFindByIdWorking()
    {
        $this->createEntityFile();

        $file = $this->fileRepo
            ->setUser($this->user)
            ->findById($this->fileId);

        $this->assertNotNull($file);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $dto = new FileNewDto();
        $dto->diskFile = 's3';
        $dto->name = Str::random(10);
        $dto->pathFile = Str::random(30);
        $dto->originalName = Str::random(30);
        $dto->updatedName = Str::random(30);
        $dto->extension = Str::random(5);
        $dto->size = random_int(0, 5000);

        $fileId = $this->fileRepo
            ->setUser($this->user)
            ->store($dto)
            ->getFileId();

        $this->assertDatabaseHas($this->fileRepo->getTableName(), [
            'id' => $fileId,
            'disk_file' => $dto->diskFile,
            'name' => $dto->name,
            'path_file' => $dto->pathFile,
            'original_name' => $dto->originalName,
            'updated_name' => $dto->updatedName,
            'extension' => $dto->extension,
            'size' => $dto->size,
        ], $this->fileRepo->getDatabaseConnection());
    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->createEntityFile();

        $dto = new FileUpdateDto();
        $dto->id = $this->fileId;
        $dto->name = Str::random(10);
        $dto->originalName = Str::random(30);
        $dto->size = random_int(0, 5000);
        $dto->extension = Str::random(5);

        $this->fileRepo
            ->setUser($this->user)
            ->update($dto);

        $this->assertDatabaseHas($this->fileRepo->getTableName(), [
            'id' => $this->fileId,
            'name' => $dto->name,
            'original_name' => $dto->originalName,
            'extension' => $dto->extension,
            'size' => $dto->size,
        ], $this->fileRepo->getDatabaseConnection());
    }

    /**
     * @test
     */
    public function isDeleteWorking()
    {
        $this->createEntityFile();

        $this->fileRepo
            ->setUser($this->user)
            ->delete($this->fileId);

        $this->assertDatabaseMissing($this->fileRepo->getTableName(), [
            'id' => $this->fileId,
        ], $this->fileRepo->getDatabaseConnection());
    }
}
