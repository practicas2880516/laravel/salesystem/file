<?php

namespace File\Test\Infrastructure\Providers\Services;

use File\Infrastructure\Providers\Dto\S3\FileCreatedDto;
use File\Infrastructure\Providers\Dto\S3\FileDeleteDto;
use File\Infrastructure\Providers\Dto\S3\FileGetContentDto;
use File\Infrastructure\Providers\Dto\S3\FileReplaceDto;
use File\Infrastructure\Providers\Dto\S3\FileUpdatedDto;
use File\Infrastructure\Providers\Services\S3ServiceProvider;
use File\Test\Base;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class S3ServiceProviderTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();
        Storage::fake('s3');
    }

    /**
     * @test
     */
    public function isGetContentWorking()
    {
        $this->createFile();

        $dto = new FileGetContentDto();
        $dto->diskFile = 's3';
        $dto->pathFile = $this->pathFile;

        $content = (App::make(S3ServiceProvider::class))
            ->getContent($dto);

        $this->assertNotNull($content);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $file = UploadedFile::fake()->image('test.png');

        $response = (App::make(S3ServiceProvider::class))
            ->store($file)
            ->getCreatedFile();

        $this->assertTrue(get_class($response) === FileCreatedDto::class);
    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->createFile();

        $dto = new FileReplaceDto();
        $dto->diskFile = 's3';
        $dto->file = UploadedFile::fake()->image('newFile.png');
        $dto->pathFile = $this->pathFile;

        $response = (App::make(S3ServiceProvider::class))
            ->update($dto)
            ->getUpdatedFile();

        $this->assertTrue(get_class($response) === FileUpdatedDto::class);
    }

    /**
     * @test
     */
    public function isDeleteWorking()
    {
        $this->createFile();

        $dto = new FileDeleteDto();
        $dto->diskFile = 's3';
        $dto->pathFile = $this->pathFile;

        (App::make(S3ServiceProvider::class))
            ->delete($dto);

        Storage::disk('s3')->assertMissing($dto->pathFile);
    }

}
