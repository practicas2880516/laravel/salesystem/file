<?php

namespace File\Test\Infrastructure\Controllers;

use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Application\Mocks\Services\AuthServiceMock;
use File\Application\Interfaces\Services\ManagerFileServiceInterface;
use File\Application\Mocks\ManagerFileServiceMock;
use File\Test\Base;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class FileControllerTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();

        $this->instance(
            AuthServiceInterface::class,
            (App::make(AuthServiceMock::class))->generateLoginUserFromTokenWorking()
        );

        $this->withHeaders([
            'Authorization' => 'Bearer ' . Str::random(30)
        ]);
    }

    /**
     * @test
     */
    public function isGetFileWorking()
    {
        $this->instance(
            ManagerFileServiceInterface::class,
            (App::make(ManagerFileServiceMock::class))->generateGetFileWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('files.getFile', [1]));

        $response->assertStatus(200);
        $response->assertJsonStructure(['message', 'code', 'success', 'file']);
    }

    /**
     * @test
     */
    public function isGetContentFileWorking()
    {
        $this->instance(
            ManagerFileServiceInterface::class,
            (App::make(ManagerFileServiceMock::class))->generateGetContentFileWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('files.getContent', [1]));

        $response->assertStatus(200);
        $response->assertJsonStructure(['message', 'code', 'success', 'content']);
    }

    /**
     * @test
     */
    public function isStoreWhenNameIsNullWorking()
    {
        $this->instance(
            ManagerFileServiceInterface::class,
            (App::make(ManagerFileServiceMock::class))->generateStoreFileWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('files.store'), [
            'name' => null,
            'document' => UploadedFile::fake()->image('test.png')
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['message', 'code', 'success', 'file_id']);
    }

    /**
     * @test
     */
    public function isStoreWhenNameIsNotNullWorking()
    {
        $this->instance(
            ManagerFileServiceInterface::class,
            (App::make(ManagerFileServiceMock::class))->generateStoreFileWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('files.store'), [
            'name' => 'File Name',
            'document' => UploadedFile::fake()->image('test.png')
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['message', 'code', 'success', 'file_id']);
    }

    /**
     * @test
     */
    public function isStoreFailing()
    {
        $this->actingAs($this->user);

        $response = $this->post(route('files.store'), [
            'name' => 'File Name',
            'document' => null
        ]);

        $response->assertStatus(302);
    }

    /**
     * @test
     */
    public function isUpdateWhenNameIsNotNullWorking()
    {
        $this->instance(
            ManagerFileServiceInterface::class,
            (App::make(ManagerFileServiceMock::class))->generateUpdateFileWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('files.update', [1]), [
            'name' => 'File Name',
            'document' => UploadedFile::fake()->image('test.png')
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['message', 'code', 'success']);
    }

    /**
     * @test
     */
    public function isUpdateWhenNameIsNullWorking()
    {
        $this->instance(
            ManagerFileServiceInterface::class,
            (App::make(ManagerFileServiceMock::class))->generateUpdateFileWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('files.update', [1]), [
            'name' => null,
            'document' => UploadedFile::fake()->image('test.png')
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['message', 'code', 'success']);
    }

    /**
     * @test
     */
    public function isUpdateFailing()
    {
        $this->actingAs($this->user);

        $response = $this->post(route('files.update', [1]), [
            'name' => null,
            'document' => null
        ]);

        $response->assertStatus(302);
    }

    /**
     * @test
     */
    public function isDeleteWorking()
    {
        $this->instance(
            ManagerFileServiceInterface::class,
            (App::make(ManagerFileServiceMock::class))->generateDeleteWorking()
        );

        $this->actingAs($this->user);

        $response = $this->delete(route('files.delete', [1]));

        $response->assertStatus(200);
        $response->assertJsonStructure(['message', 'code', 'success']);
    }
}
