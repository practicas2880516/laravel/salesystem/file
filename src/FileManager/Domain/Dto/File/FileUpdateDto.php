<?php

namespace File\Domain\Dto\File;

use File\Domain\Dto\BaseDto;

class FileUpdateDto extends BaseDto
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string|null
     */
    public ?string $name = null;

    /**
     * @var string
     */
    public string $originalName;

    /**
     * @var float
     */
    public float $size;

    /**
     * @var string
     */
    public string $extension;
}
