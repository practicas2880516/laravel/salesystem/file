<?php

namespace File\Domain\Dto\File;

use File\Domain\Dto\BaseDto;
use Illuminate\Http\UploadedFile;

class UpdateFileDto extends BaseDto
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string|null
     */
    public ?string $name = null;

    /**
     * @var UploadedFile
     */
    public UploadedFile $file;
}
