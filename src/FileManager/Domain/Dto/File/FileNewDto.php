<?php

namespace File\Domain\Dto\File;

use File\Domain\Dto\BaseDto;

class FileNewDto extends BaseDto
{
    /**
     * @var string
     */
    public string $diskFile;

    /**
     * @var string|null
     */
    public ?string $name = null;

    /**
     * @var string
     */
    public string $pathFile;

    /**
     * @var string
     */
    public string $originalName;

    /**
     * @var string
     */
    public string $updatedName;

    /**
     * @var float
     */
    public float $size;

    /**
     * @var string
     */
    public string $extension;
}
