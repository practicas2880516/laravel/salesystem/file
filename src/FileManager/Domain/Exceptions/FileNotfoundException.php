<?php

namespace File\Domain\Exceptions;

class FileNotfoundException extends \Exception
{
    /**
     * @var int
     */
    protected $code = 404;

    /**
     * @var string
     */
    protected $message = 'File not found';
}
