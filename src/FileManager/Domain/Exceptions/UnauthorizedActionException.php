<?php

namespace File\Domain\Exceptions;

class UnauthorizedActionException extends \Exception
{
    protected $code = 401;
    protected $message = 'Unauthorized action';
}
