<?php

namespace Auth\Test\Infrastructure\Providers\Services;

use Auth\Infrastructure\Interfaces\Providers\Services\AuthServiceProviderInterface;
use Auth\Test\Base;
use Illuminate\Support\Facades\App;

class AuthServiceProviderTest extends Base
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function isGetUserByTokenWorking()
    {
        $user = (App::make(AuthServiceProviderInterface::class))
            ->getUserByToken('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6IkFkbWluaXN0cmFkb3IiLCJlbWFpbCI6ImpoYW52ZWdhMDFAb3V0bG9vay5jb20iLCJleHAiOjE3MDM2NDM3NDN9.H8B-__-SNGdJGltohAgWruTZ8FSO9GUxcPzOAWmz0MQ');

        $this->assertIsArray($user);
    }
}
