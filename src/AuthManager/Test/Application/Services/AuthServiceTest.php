<?php

namespace Auth\Test\Application\Services;

use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Infrastructure\Interfaces\Providers\Services\AuthServiceProviderInterface;
use Auth\Infrastructure\Mocks\Providers\Services\AuthServiceProviderMock;
use Auth\Test\Base;
use Illuminate\Support\Facades\App;

class AuthServiceTest extends Base
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function isLoginUserFromTokenWorking()
    {
        $this->instance(
            AuthServiceProviderInterface::class,
            (App::make(AuthServiceProviderMock::class))
                ->generateGetUserByTokenWorking()
        );

        (App::make(AuthServiceInterface::class))
            ->loginUserFromToken('token');

        $this->assertNotNull(auth()->user());
    }
}
