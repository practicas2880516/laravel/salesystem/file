<?php

namespace App\Providers;

use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Application\Services\AuthService;
use Auth\Infrastructure\Interfaces\Providers\Services\AuthServiceProviderInterface;
use Auth\Infrastructure\Providers\Services\AuthServiceProvider;
use File\Application\Interfaces\Services\ManagerFileServiceInterface;
use File\Application\Interfaces\Services\StorageFileServiceInterface;
use File\Application\Services\ManagerFileService;
use File\Infrastructure\Interfaces\Repositories\Files\FileRepositoryInterface;
use File\Infrastructure\Providers\Services\S3ServiceProvider;
use File\Infrastructure\Repositories\Files\FileRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //Providers
        $this->app->bind(AuthServiceProviderInterface::class, AuthServiceProvider::class);

        //Services
        $this->app->bind(AuthServiceInterface::class, AuthService::class);
        $this->app->bind(ManagerFileServiceInterface::class, ManagerFileService::class);
        $this->app->bind(StorageFileServiceInterface::class, S3ServiceProvider::class);

        // Repositories
        $this->app->bind(FileRepositoryInterface::class, FileRepository::class);
    }
}
